#  Полное описание обработки событий
## Отбор событий
### Электроны
* 4-импульс : `SetPtEtaPhiM(gsf80_pt->at(j), gsf_eta->at(j), gsf_phi->at(j), 0.000511)`
* заряд `gsf_charge->at(j)`
* eta_sc `gsf_sc_eta->at(j)`
* Tight ID (читается напрямую из дерева, сам я не вычисляю этот флаг): `gsf_VIDTight`
* dxy, dz: `gsf_dxy_firstPVtx->at(j)`, `gsf_dz_firstPVtx->at(j)`
* отбираются tight электроны, с `pT > 20`, `|eta| < 2.4`, не попавшие в gap (gap: `1.566 > |eta_sc| > 1.4442`)
* для barrel-электронов (`|eta_sc| < 1.4442`) требуем: `|dxy| < 0.05`, `|dz| < 0.10`
* для endcap-электронов (`|eta_sc| > 1.566`) требуем: `|dxy| < 0.10`, `|dz| < 0.20`

### Мюоны
* Умножаем `pT` на множитель (rochester corrections, 2016.v3)
```
rc.kScaleAndSmearMC(mu_gt_charge->at(j), pt, mu_gt_eta->at(j), mu_gt_phi->at(j),
                    mu_trackerLayersWithMeasurement->at(j), gRandom->Rndm(), gRandom->Rndm(),
                    0, 0);
```

* 4-импульс: `SetPtEtaPhiM(pt, mu_gt_eta->at(j), mu_gt_phi->at(j), 0.10566)`
* заряд `mu_gt_charge->at(j)`
* изоляция `mu_pfIsoDbCorrected04->at(j)`
* обрезание по dxy, dz не делается
* отбираются tight мюоны, с `pT > 20`, `|eta| < 2.4`, с `iso < 0.15`

### Струи
* 4-импульс: `SetPtEtaPhiE(jet_pt->at(j), jet_eta->at(j), jet_phi->at(j), jet_energy->at(j))`
* b-tag: `jet_CSVv2->at(j) > 0.8484`
* loose id: `jet_isJetIDLoose->at(j)`
* `pT > 30`, `|eta| < 2.4`
* Струи "чистятся": убираются те, для которых есть лептон (selected, то есть после всех катов) с `dR(lepton, jet) <= 0.4`

### MET
* `TLorentzVector(MET_T1Txy_Px, MET_T1Txy_Py, 0, 0)`
* Следующие триггеры должны все сработать, чтобы MET был правильным: 
```
  trig_Flag_BadChargedCandidateFilter_accept && trig_Flag_BadPFMuonFilter_accept &&
  trig_Flag_EcalDeadCellTriggerPrimitiveFilter_accept &&
  trig_Flag_globalTightHalo2016Filter_accept && trig_Flag_goodVertices_accept &&
  trig_Flag_HBHENoiseFilter_accept && trig_Flag_HBHENoiseIsoFilter_accept
```

### Триггер
Логическое ИЛИ следующих триггеров (для EMu):
```
  trig_HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_accept
  trig_HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_accept
  trig_HLT_Ele27_WPTight_Gsf_accept
  trig_HLT_IsoMu24_accept 
  trig_HLT_IsoTkMu24_accept
```

## Мода распада ttbar (в отборе напрямую не участвует)
* Считаем число лептонов (e, mu, tau) со статусом 1 в ветке `LHE_pdgid`; отбрасываем события в которых > 2 лепотонов со статусом 1 (таких пока не встречал)

## Вычисление веса события
* Началный вес события - `mc_w_sign` (+1 или -1)
* Если в событии есть b-струи, то для каждой из них считается b-tagging SF
    * Файл `CSVv2_Moriond17_B_H.csv`, operating point: medium, systematics: central, measurement type: mujets
    * Полученные SF перемножаются
* Мюоонные SF:
    * Muon ID SF:
        * Из файла `MuID_EfficienciesAndSF_BCDEF.root` берётся гистограмма `MC_NUM_TightID_DEN_genTracks_PAR_pt_eta/abseta_pt_ratio`
        * Находится бин, соответствующих pT и |eta| мюона; если pT > максимального (120 ГэВ) то событие отбрасывается; при этом если pT > 200 GeV, то считается что pT = 120 GeV
            * это самая сомнительная часть кода!
        * Значение в этом бине - это ID SF для эпох B-F (`mu_id_sf_bcdef`)
        * Процесс повторяется с файлом `MuID_EfficienciesAndSF_GH.root`, получаем значение ID SF для эпох G-H (`mu_id_sf_gh`)
    * Вычисление Muon ISO SF полносью повторят вычисление ID SF, только берутся файлы MuIso_EfficienciesAndSF_BCDEF.root, MuIso_EfficienciesAndSF_GH.root (`mu_iso_sf_bcdef`, `mu_iso_sf_gh`)
        * Итоговый SF для мюона: `mu_sf = mu_id_sf_bcdef * mu_iso_sf_bcdef * lumEraBCDEF / lumEraBCDEFGH + mu_id_sf_gh * mu_iso_sf_gh * lumEraGH / lumEraBCDEFGH`
            * `lumEraBCDEF = 19255482132.2`, `lum_era_gh = 16290016931.8`, `lum_era_bcdefgh = 35545499064.0`
            * Эти множители соответствуют luminosity эпох BCDEF, GH и суммарной BCDEFGH
        * SF от разных мюонов перемножаются между собой
* Электронный SF
    * Берётся файл `egammaEffi.txt_EGM2D.root`, гистограмма `EGamma_SF2D`, в которой лежат SF в зависимости от eta_sc и pT; нет проверки что pT не выходит за пределы оси (500 ГэВ)
* Pileup SF
    * есть две гистограммы: распределение числа взаимодейтсвий в данных и в моём сэмпле MC (считаю один раз до запуска самого анализа - берётся переменная `mc_trueNumInteractions`)
    * на основании этих двух распределений стандартный CMSSW модуль считает SF (по сути - отношение этих гистограмм в бине, равном `mc_trueNumInteractions` в этом событии)
* Trigger SF
    * Файл `TriggerSF_emu2016_pt.root`, гистограмма `lepton_pt_2D_sf` - SF в зависимости от pT leading и sub-leading электронов
* Все SF перемножаются между собой и умножаются на `mc_w_sign`, полученное значение становится весом события

## Отбор событий
1. Загрузка события и вычисление его веса (на этом этапе убираются события с мюонами с pT > 120)
2. Проверка "правильности" MET
3. Не менее двух лептонов, из которых первые два (с наибольшими pT) должны быть разного знака, у ведущего лептона `pT > 25`, масса дилептона `m(ll) > 20`
4. Определяем, что за событие у нас - EE, EMu, MuMu
5. (только для EE и MuMu) отбрасываем событие если `76 <= m(ll) <= 106` или `MET <= 40`
6. Проверяем триггер
7. Считаем сколько у нас струй и b-струй

Для проверки выходов (yields) количества событий нормируются на интегральную светимость по формуле N = N_sel * sigma * L / N_tot . Здесь N_sel - число отобранных событий, sigma - сечение процесса (взято из таб. 3 в AN, в pb),
L - интегральная светимость (35867 pb-1), N_tot - полное число событий в сэмпле, N - нормированное на светимость число событий.